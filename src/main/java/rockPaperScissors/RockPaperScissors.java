package rockPaperScissors;

import java.text.BreakIterator;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner14;

import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    static List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    //user choice
    public String userInput(){
        while (true){
        String humanInput =readInput("Your choice (Rock/Paper/Scissors)?");
        /*Scanner scanner = new Scanner(System.in);
        System.out.println("Your choice (Rock/Paper/Scissors)?");
        String humanInput = scanner.nextLine();
        */
        String humanChoice = humanInput.toLowerCase();


        //checking if the human player submitted a valid value for humanChoice
        if (rpsChoices.contains(humanChoice)){
            return humanChoice;
        }
        else{
            System.out.println("I do not understand "+humanChoice+". Could you try again?");
        }
        
    }
    }
    //using random number to get input from computer
    public static String computerInput(){
        Random random = new Random();
        int randomNum = random.nextInt(3);

        String computerChoice;
        if (randomNum == 0){
            return computerChoice = "rock";
        }
        else if (randomNum == 1){
            return computerChoice = "paper";
        }
        else {
            return computerChoice = "scissors";
        }
    }

    //making a method for the winner and scores
    boolean winner(String choice1, String choice2){
        if (choice1.equals("rock")){
            return choice2.equals("scissors");
        }
     else if (choice1.equals("paper")){
            return choice2.equals("rock");
        }
        else {
            return choice2.equals("paper");
        }
    }

     //method for checking wether the human player wishes to continue or not
     public String continues(){
        while (true){
            String continueYN = readInput("Do you wish to continue playing? (y/n)?");
            /*Scanner scanner = new Scanner(System.in);
            System.out.println("Do you wish to continue playing? (y/n)?");
            String continueYN = scanner.nextLine();
            */
            String continueAnswer = continueYN.toLowerCase();
            List<String> validAnswer = Arrays.asList("y", "n");
            if (validAnswer.contains(continueAnswer)){
                return continueAnswer;
            }
            else{
                System.out.println("I do not understand "+continueAnswer+". Could you try again?");
            }
        }
    }

    public void run() {
    while (true){
        System.out.println("Let's play round "+roundCounter);

            /*leftover tests from checking if methods worked as intended
                System.out.println("human chose "+userInput()+"!");
                System.out.println("Computer chose "+computerInput()+"!");
            */

        //getting inputs from methods and preparing a string for printing later accordingly, referenced from the python code
        String humanChoice = userInput();
        String computerChoice = computerInput();
        String choiceString = ("Human chose "+humanChoice+", computer chose "+computerChoice+".");
        
        //checking for the winner and prints appropriate result string
        if (winner(humanChoice, computerChoice)){
            System.out.println(choiceString + " Human wins!");
            humanScore+=1;
        }
        else if (winner(computerChoice, humanChoice)){
            System.out.println(choiceString + " Computer wins!");
            computerScore+=1;
        }
        else{
            System.out.println(choiceString + " It's a tie!");
        }
        //prints score so far and asks if player wishes to continue
        System.out.println("Score: human "+humanScore+", computer "+computerScore);

        String continuesAnswer = continues();
        if ((continuesAnswer.equals("n"))){
            break;
        }
        else{
            roundCounter+=1;
        }
        }
        System.out.println("Bye bye :)");
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
